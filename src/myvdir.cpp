/* Copyright (C) 2029 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "myvdir.h"

#include <iostream>

#include <assert.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

bool verbose{false};

// Virtual directory functions. They could serve any data from
// anywhere, e.g. typically memory for description files, or a network
// connection for some streaming data. Here to make things simpler, we
// use an actual file system directory is our virtual dir. To make
// things simpler we don't even check the paths (the lib does it
// anyway, that's how it knows to call the vdcallback functions).

// getinfo is ALWAYS called first if we are going to do something with
// a file. The request_cookie set here will be be sent to all further
// calls for the same request.
static int vdcallback_getinfo(
    const char *filename, struct File_Info *info, const void *cookie
    , const void **request_cookiep
    )
{
    if (verbose)
        std::cerr << "vdcallback_getinfo: [" << filename << "]\n";
    struct stat st;
    if (stat(filename, &st)< 0) {
        perror("stat");
        return -1;
    }
    info->file_length = st.st_size;
    info->last_modified = st.st_mtime;
    info->is_directory = S_ISDIR(st.st_mode);
    info->is_readable = (access(filename, R_OK) == 0);
    info-> content_type = "application/octet-stream";
    *request_cookiep = (void*)0x1234;
    return 0;
}

static UpnpWebFileHandle vdcallback_open(
    const char *filename, enum UpnpOpenFileMode Mode, const void *cookie,
    const void *request_cookie)
{
    if (verbose)
        std::cerr << "vdcallback_open: [" << filename << "] rcookie " <<
            request_cookie << "\n";
    int fd = open(filename, 0);
    if (fd < 0) {
        perror("open");
        return nullptr;
    }
    int *ret = (int*)malloc(sizeof(int));
    *ret = fd;
    return ret;
}

static int vdcallback_read(
    UpnpWebFileHandle fdp, char *buf, size_t buflen, const void *cookie,
    const void *request_cookie)
{
    int fd = *(int*)fdp;
    //std::cerr << "vdcallback_read: fd "  << fd << " count " << buflen << "\n";
    return read(fd, buf, buflen);
}

static int vdcallback_write(
    UpnpWebFileHandle fdp, char *buf, size_t buflen, const void *cookie,
    const void *request_cookie)
{
    std::cerr << "vdcallback_write !!!!!\n";
    return -1;
}

int vdcallback_seek(
    UpnpWebFileHandle fdp, int64_t offset, int origin, const void *cookie,
    const void *request_cookie)
{
    int fd = *(int*)fdp;
    if (verbose)
        std::cerr << "vdcallback_seek: fd "  << fd << " offset " << offset <<
            " origin " << origin << "\n";
    if (lseek(fd, offset, origin) >= 0) {
        return 0;
    }
    return -1;
}

static int vdcallback_close(UpnpWebFileHandle fdp, const void *cookie,
                     const void *request_cookie)
{
    int fd = *(int*)fdp;
    if (verbose)
        std::cerr << "vdcallback_close: fd "  << fd << " rcookie " <<
            request_cookie << "\n";
    assert(request_cookie == (void*)0x1234);
    close(fd);
    free(fdp);
    return 0;
}

struct UpnpVirtualDirCallbacks mycallbacks {
    vdcallback_getinfo,
        vdcallback_open,
        vdcallback_read,
        vdcallback_write,
        vdcallback_seek,
        vdcallback_close
};
