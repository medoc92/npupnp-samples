/* Copyright (C) 2029 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
  Exercising the npupnp library webserver functions. We can serve files
  both through the direct file system server and through the virtual
  directory interface, which is set here to also directly serve from
  the same file system directory. Which method will be used depends on
  the request path. If the root given on the command line, curl
  http://host:port/somefile would use the file system interface
  (fetching from the root), but curl http://host:port/tmp/somefile
  would use the virtual one (because the /tmp path is a virtual
  directory).

  The lib always tests for a virtual directory first, so if the top of
  the path matches one, the virtual interface will always be used.
*/

#include <iostream>
#include <string>
#include <unistd.h>
#include <errno.h>
#include <getopt.h>
#include <assert.h>

#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>

#include "myvdir.h"

void callfailed(const char *call, int code)
{
    std::cerr << call << " failed. code " << code << ": " <<
        UpnpGetErrorMessage(code) << "\n";
}


static char *thisprog;
static char usage[] =
"[-n|-s] <rootpath> : serve files from directory rootpath (must exist)\n"
"  if <somefile> exists in <rootpath>:\n"
"    - requesting /<somefile> will use the direct file system interface.\n"
"    - requesting /<rootpath>/<somefile> will use the virtualdir interface\n"
" Option -n will suppress HOST header validation\n"
" Option -s will reject bad HOST headers instead of redirecting\n"
" Option -c will activate the hostname-validating routine (which will randomly\n"
"       validate or not depending if the time() is odd or even\n"
;
int op_flags;
#define OPT_n 0x1
#define OPT_s 0x2
#define OPT_c 0x4
static struct option long_options[] = {
    {0, 0, 0, 0}
};

static void
Usage(void)
{
    fprintf(stderr, "Usage: %s %s", thisprog, usage);
    exit(1);
}

int host_validate_cb(const char *hostname, void *)
{
    std::cerr << "Validating " << hostname << " ... ";
    if (time(0) & 1) {
        std::cerr << " Ok\n";
        return UPNP_E_SUCCESS;
    }
    std::cerr << " Nope\n";
    return UPNP_E_BAD_HTTPMSG;
}

int main(int argc, char **argv)
{
    thisprog = argv[0];
    int ret;
    while ((ret = getopt_long(argc, argv, "nsc", long_options, NULL)) != -1) {
        switch (ret) {
        case 'n': op_flags |= OPT_n; break;
        case 's': op_flags |= OPT_s; break;
        case 'c': op_flags |= OPT_c; break;
        default:
            Usage();
        }
    }
    if (optind != argc - 1) 
        Usage();
    const char *public_directory = argv[optind++];

    if (access(public_directory, 0) != 0) {
        Usage();
    }
    int errcode;

    unsigned int flags = 0;
    if (op_flags & OPT_n) {
        flags |= UPNP_FLAG_NO_HOST_VALIDATE;
    } else if (op_flags & OPT_s) {
        flags |= UPNP_FLAG_REJECT_HOSTNAMES;
    }
    errcode = UpnpInitWithOptions(nullptr, 0, flags, UPNP_OPTION_END);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpInitWithOptions", errcode);
        return 1;
    }
    if (op_flags & OPT_c) {
        UpnpSetWebRequestHostValidateCallback(host_validate_cb, nullptr);
    }

    UpnpSetLogLevel(UPNP_INFO);
    UpnpSetLogFileNames("", "");
    UpnpInitLog();

    std::cout << "Webserver " << getpid() << " IP " <<
        UpnpGetServerIpAddress() << " port " << UpnpGetServerPort() << std::endl;

    errcode = UpnpSetWebServerRootDir(public_directory);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpSetWebServerRootDir", errcode);
        return 1;
    }
    errcode = UpnpSetVirtualDirCallbacks(&mycallbacks);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpSetVirtualDirCallbacks", errcode);
        return 1;
    }
    errcode = UpnpAddVirtualDir(public_directory, nullptr, nullptr);

    pause();

    errcode = UpnpFinish();
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpFinish", errcode);
        return 1;
    }
    
    return 0;
}
