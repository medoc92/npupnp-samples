/* Copyright (C) 2029 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

// Exercising the control-point side of libnpupnp.

#include "utils.h"

#include <iostream>
#include <sstream>
#include <string>
#include <mutex>
#include <condition_variable>

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>

#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>
#include <upnpdescription.h>

void callfailed(const char *call, int code)
{
    std::cerr << call << " failed. code " << code << ": " <<
		UpnpGetErrorMessage(code) << "\n";
}

// Our ctrlpt handle and other variables. Need these in several
// places. Of course, in a real implementation, this would be stored
// in a data structure pointed to by the cookie argument which we set
// when registering the device, and receive in the callbacks. Also
// several services, etc.  This is just a barebones example.
static UpnpClient_Handle the_handle;
static std::string deviceudn;
static bool devicefound{false};
static struct Upnp_Discovery devdetails;
static std::string service_ctlurl;
static std::string service_evturl;
static std::string service_type;

static std::mutex devicemutex;
static std::condition_variable devicecv;

// Call an action with args, receive and display the result. This can
// block, so would probably run in a thread in a real app.
int runaction(const std::string& ActionName,
              const std::vector<std::pair<std::string, std::string>>& args)
{
    std::cerr << "runaction. service URL " << service_ctlurl << " action: " << ActionName << "\n";

    int errorcode;
    std::string errdesc;
    std::vector<std::pair<std::string, std::string>> responsedata;
    int res = UpnpSendAction(
        the_handle, "" /* soap header data */, service_ctlurl, service_type,
        ActionName, args, responsedata, &errorcode, errdesc);
    if (res != 0) {
        callfailed("UpnpSendAction", res);
        return res;
    }
    std::cerr << "Response arg cnt: " << responsedata.size() << std::endl;
    for (const auto& entry : responsedata) {
        std::cerr << "[" << entry.first << "]->[" << entry.second << "]\n";
    }
    return UPNP_E_SUCCESS;
}

// This gets called every time something happens in the lib: device
// advertisements, search timeout, or service events. We run in a
// separate thread, and need to synchronize with the main one.
static int g_lib_callback(
    Upnp_EventType evttype, const void *event, void *cookie)
{
    //std::cerr << "g_lib_callback. evttype " << evttype <<
	//	" event " << (void*)event << " cookie " << cookie << "\n";

    switch (evttype) {
    case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
    case UPNP_DISCOVERY_SEARCH_RESULT:
    case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
        // Process messages about devices and services existence. If
        // our designated device is seen, we store the relevant
        // details for the rest of the program to use, and wake
        // ourselves. The main thread is will wait until then (or the
        // search timeout). Different threads: this needs synchronisation.
    {
        struct Upnp_Discovery *dscp = (struct Upnp_Discovery*)event;
        if (evttype == UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE) {
            std::cerr << "Got upnp bye bye for " << dscp->DeviceId << "\n";
            return 0;
        }
        std::unique_lock<std::mutex> devlock(devicemutex);
        std::cerr << "Got upnp alive/searchresult for " << dscp->DeviceId <<
            " servicetype [" << dscp->ServiceType << "]" << std::endl;
        if (deviceudn == dscp->DeviceId) {
            if (!devicefound) {
                devicefound = true;
                devdetails = *dscp;
                devicecv.notify_all();
            }
        }
    }
    break;

    case UPNP_DISCOVERY_SEARCH_TIMEOUT:
    {
        // The lib signals the end of the search window. If the device
        // was not found, wake-up the main thread (else it's running).
        std::cerr << "Got upnp search timeout (end of search window)\n";
        std::unique_lock<std::mutex> devlock(devicemutex);
        if (!devicefound) {
            devicecv.notify_all();
        }
    }
    break;

    case UPNP_EVENT_RECEIVED:
    {
        // General events from the services we subscribed to
        struct Upnp_Event *evp = (struct Upnp_Event *)event;
        std::cerr << "Got upnp event for subscription " << evp->Sid <<
            " seq " << evp->EventKey << " Changed:\n";
        for (const auto& entry : evp->ChangedVariables) {
            std::cerr << "[" << entry.first << "]->[" << entry.second << "]\n";
        }
    }
    break;

    default:
        std::cerr << "Got event type " << evttype << "\n";
    }

    return 0;
}

static char *thisprog;
static char usage[] =
"<deviceudn> : test npupnp test device control\n"
;
static void
Usage(void)
{
    fprintf(stderr, "Usage: %s %s", thisprog, usage);
    exit(1);
}

int main(int argc, char **argv)
{
    thisprog = *argv++; argc--;
    if (argc < 1) {
        Usage();
    }
    deviceudn = *argv++; argc--;
    
    ////// Initialize library.
    int errcode = UpnpInit2(nullptr, 0);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpInit2", errcode);
        return 1;
    }
    UpnpSetLogLevel(UPNP_ERROR);
    UpnpInitLog();

    std::cerr << "IP " << UpnpGetServerIpAddress() << " port " <<
        UpnpGetServerPort() << std::endl;


    //// Register our callback and get a client handle
    errcode = UpnpRegisterClient(g_lib_callback, nullptr, &the_handle);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpRegisterClient", errcode);
        return 1;
    }


    //// Launch search, then wait. Our callback function will wake us
    // up if it hears from the device specified on the command line
    errcode = UpnpSearchAsync(the_handle, 2, "upnp:rootdevice", nullptr);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpSearchAsync", errcode);
        return 1;
    }
    {
        std::unique_lock<std::mutex> lock(devicemutex);
        if (!devicefound) {
            devicecv.wait(lock);
            if (!devicefound) {
                std::cerr << "Device was not found\n";
                return 1;
            }
        }
    }


    //// Retrieve and parse the device description. We need it
    // to get information about how to reach the service (e.g.:
    // service control and event URLs)
    std::cerr << "Downloading description document " << devdetails.Location
              << std::endl;
    std::string devicedescription, cttype;
    errcode = UpnpDownloadUrlItem(
		devdetails.Location, devicedescription, cttype);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpSearchAsync", errcode);
        return 1;
    }

    // libupnpp has a description parser, just use it.
    UPnPDeviceDesc parsed_desc(devdetails.Location, devicedescription);
    if (!parsed_desc.ok) {
        std::cerr << "Device description parse failed for [" <<
            devicedescription << std::endl;
        return 1;
    }
    //// Our test device has exactly one service. Retrieve the service
    // details and store in globals for use by our runaction() routine.
    if (parsed_desc.services.size() != 1) {
        std::cerr << "Wrong service count " <<parsed_desc.services.size()<<"\n";
        return 1;
    }
    service_type = parsed_desc.services[0].serviceType;
    service_ctlurl = parsed_desc.URLBase + parsed_desc.services[0].controlURL;
    service_evturl = parsed_desc.URLBase + parsed_desc.services[0].eventSubURL;


    ///// Subscribe to the service events. We should get one when we
    // set the value further down.
    int timeout = 3600;
    Upnp_SID subsid;
    errcode = UpnpSubscribe(
        the_handle, service_evturl.c_str(), &timeout, subsid);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpSubscribe", errcode);
        return 1;
    }


    ///// Run a few actions
    runaction("GetValue", std::vector<std::pair<std::string, std::string>>());
    std::vector<std::pair<std::string, std::string>> args{{"Value", "43"}};
    runaction("SetValue", args);
    runaction("GetValue", std::vector<std::pair<std::string, std::string>>());


    ///// Clean up
    errcode = UpnpUnRegisterClient(the_handle);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpRegisterClient", errcode);
        return 1;
    }
    errcode = UpnpFinish();
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpFinish", errcode);
        return 1;
    }
    return 0;
}
