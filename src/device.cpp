/* Copyright (C) 2029 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "myvdir.h"
#include "utils.h"

#include <iostream>
#include <sstream>
#include <string>

#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>

void sigcatcher(int)
{
    return;
}

static std::string deviceDescription {
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    "<root xmlns=\"urn:schemas-upnp-org:device-1-0\">"
    "  <specVersion>"
    "    <major>1</major>"
    "    <minor>1</minor>"
    "  </specVersion>"
    "  <device>"
    "    <deviceType>urn:jfd:testdevice:1</deviceType>"
    "    <friendlyName>npupnp-test-device</friendlyName>"
    "    <manufacturer>jfd</manufacturer>"
    "    <modelName>jfd-webserver</modelName>"
    "    <UDN>@UDN@</UDN>"
    "    <serviceList>"
    "      <service>"
    "        <serviceType>urn:jfd:service:MyService:1</serviceType>"
    "        <serviceId>urn:jfd:serviceId:MyService</serviceId>"
    "        <SCPDURL>@SCPDURL@</SCPDURL>"
    "        <controlURL>/ctl/MyService</controlURL>"
    "        <eventSubURL>/evt/MyService</eventSubURL>"
    "      </service>"
    "    </serviceList>"
    "  </device>"
    "</root>"
};

// Our service has two actions, to set and retrieve a value. Who needs more?
const static std::string serviceDescription {
    "<?xml version=\"1.0\" encoding=\"utf-8\"?>"
    "<scpd xmlns=\"urn:schemas-upnp-org:service-1-0\">"
    "  <specVersion>"
    "    <major>1</major>"
    "    <minor>1</minor>"
    "  </specVersion>"
    "  <actionList>"
    "    <action>"
    "      <name>GetValue</name>"
    "      <argumentList>"
    "        <argument>"
    "          <name>Value</name>"
    "          <direction>out</direction>"
    "          <relatedStateVariable>Value</relatedStateVariable>"
    "        </argument>"
    "      </argumentList>"
    "    </action>"
    "    <action>"
    "      <name>SetValue</name>"
    "      <argumentList>"
    "        <argument>"
    "          <name>Value</name>"
    "          <direction>in</direction>"
    "          <relatedStateVariable>Value</relatedStateVariable>"
    "        </argument>"
    "      </argumentList>"
    "    </action>"
    "  </actionList>"
    "  <serviceStateTable>"
    "    <stateVariable sendEvents=\"yes\">"
    "      <name>Value</name>"
    "      <dataType>ui4</dataType>"
    "    </stateVariable>"
    "  </serviceStateTable>"
    "</scpd>"
};

void callfailed(const char *call, int code)
{
    std::cerr << call << " failed. code " << code << ": " <<
		UpnpGetErrorMessage(code) << "\n";
}


// Our device handle. Need this in several places. Of course, in a
// real implementation, this would be stored in a data structure
// pointed to by the cookie argument which we set when registering the
// device, and receive in the callbacks.
static UpnpDevice_Handle the_handle;

// Because we share a virtualdir implementation with the webserver
// test program, and because this is actually file-system-based, we
// use a temporary directory to store the HTTP-downloadable
// description files. We do not use the local file serving capability from
// the WEB server (see smallsamps/upnp_fsdevice for this).
static const char *cdirtemplate{"/tmp/npupnpXXXXXX"};
static char *vdirnm;

static const std::string myudn{"uuid:jfd-npupnp-test-on-y"};

// Service description XML path (this points inside the temp dir above).
static std::string sdpath;

static void cleanup()
{
    if (vdirnm) {
        if (!sdpath.empty()) {
            unlink(sdpath.c_str());
        }
        rmdir(vdirnm);
    }
}

static int theValue = 42;

static std::string thevalueasstr()
{
    std::ostringstream ostr;
    ostr << theValue;
    return ostr.str();
}
    
static int runaction(Upnp_Action_Request *arp)
{
    std::cerr << "Action request: action " << arp->ActionName << " UDN " <<
        arp->DevUDN << " ServiceID " << arp->ServiceID << " arg cnt " <<
        arp->args.size() << "\n";

    if (!strcmp("GetValue", arp->ActionName)) {
        if (arp->args.size() != 0) {
            std::cerr << "'Value' action should have no args\n";
            return UPNP_E_INVALID_PARAM;
        }
        arp->resdata.push_back(
        std::pair<std::string, std::string>("Value", thevalueasstr()));

    } else if (!strcmp("SetValue", arp->ActionName)) {
        if (arp->args.size() != 1) {
            std::cerr << "'Value' action should have no args\n";
            return UPNP_E_INVALID_PARAM;
        }
        if (arp->args[0].first != "Value") {
            std::cerr << "'Value' action bad arg name [" << arp->args[0].first << "]\n";
            return UPNP_E_INVALID_PARAM;
        }
        theValue = atoi(arp->args[0].second.c_str());

        // We just presumably changed the value of our variable, so
        // generate an event.
        const char *names[]{"Value"};
        const char *values[]{thevalueasstr().c_str()};
        UpnpNotify(the_handle, arp->DevUDN, arp->ServiceID, names, values, 1);

    } else {
        std::cerr << "BAD ACTION NAME [" << arp->ActionName << "]\n";
        return UPNP_E_INVALID_PARAM;
    }
    return UPNP_E_SUCCESS;
}

// Process subscription request. We send all the state variable values
// through the UpnpAcceptSubscription function.
static int subscriptionrequest(struct Upnp_Subscription_Request *srp)
{
    std::cerr << "Subscription request: UDN " << srp->UDN << " serviceId " <<
        srp->ServiceId << " Subscription ID " << srp->Sid << "\n";

    const char *names[]{"Value"};
    const char *values[]{thevalueasstr().c_str()};
    return UpnpAcceptSubscription(the_handle, srp->UDN, srp->ServiceId, names,
                                  values, 1, srp->Sid);
}    

// This gets called every time something happens in the lib.
static int g_lib_callback(
    Upnp_EventType evttype, const void *event, void *cookie)
{
    std::cerr << "g_lib_callback. evttype " << evttype <<
		" event " << (void*)event << " cookie " << cookie << "\n";

    switch (evttype) {

    case UPNP_CONTROL_ACTION_REQUEST:
    {
        struct Upnp_Action_Request *arp = (struct Upnp_Action_Request *)event;
        return runaction(arp);
    }
    break;

    case UPNP_EVENT_SUBSCRIPTION_REQUEST:
    {
        struct Upnp_Subscription_Request *srp =
            (struct Upnp_Subscription_Request *)event;
        return subscriptionrequest(srp);
    }

    default:
        std::cerr << "Got event type " << evttype << "\n";
    }

    return 0;
}

static char *thisprog;
static char usage[] =
"[no arguments] : test device\n"
;
static void
Usage(void)
{
    fprintf(stderr, "Usage: %s %s", thisprog, usage);
    exit(1);
}

bool storeDescription()
{
    vdirnm = strdup(cdirtemplate);
    if (nullptr == mkdtemp(vdirnm)) {
        perror("mkdtemp");
        return false;
    }
    sdpath = std::string(vdirnm) + '/' + "MyService.xml";
    int fd = open(sdpath.c_str(), O_WRONLY|O_CREAT, 0444);
    if (fd < 0) {
        std::cerr << " Error opening " << sdpath << " : ";
        perror("open");
        return false;
    }
    if (write(fd, serviceDescription.c_str(), serviceDescription.size()) !=
        int(serviceDescription.size())) {
        perror("write");
        close(fd);
        return false;
    }
    close(fd);
    return true;
}

int main(int argc, char **argv)
{
    thisprog = *argv++;
    argc--;
    if (argc != 0) {
        Usage();
    }

    atexit(cleanup);
    signal(SIGINT, sigcatcher);
    signal(SIGTERM, sigcatcher);

    // Store the service description so that it can be downloaded through the
    // web server.
    if (!storeDescription()) {
        return 1;
    }

    // Initialize library. This starts the web server.
    int errcode = UpnpInit2(nullptr, 0);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpInit2", errcode);
        return 1;
    }
    UpnpSetLogLevel(UPNP_DEBUG);
    UpnpInitLog();

    std::cerr << "IP " << UpnpGetServerIpAddress() << " port " <<
        UpnpGetServerPort() << " UDN " << myudn << "\n";

	errcode = UpnpSetVirtualDirCallbacks(&mycallbacks);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpSetVirtualDirCallbacks", errcode);
        return 1;
    }
	errcode = UpnpAddVirtualDir(vdirnm, nullptr, nullptr);

    // Declare our device. The device description includes the URL for
    // the service description of our one and only service.
    str_replace1(deviceDescription, "@UDN@", myudn);
    str_replace1(deviceDescription, "@SCPDURL@", sdpath);
    errcode = UpnpRegisterRootDevice2(
        UPNPREG_BUF_DESC, deviceDescription.c_str(), deviceDescription.size(), 
        0, g_lib_callback, nullptr, &the_handle);
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpRegisterRootDevice2", errcode);
        return 1;
    }

    // And just let the lib work.
    pause();

    errcode = UpnpFinish();
    if (errcode != UPNP_E_SUCCESS) {
        callfailed("UpnpFinish", errcode);
        return 1;
    }
    
    return 0;
}
