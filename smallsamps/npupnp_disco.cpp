/* Copyright (C) 2017-2019 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <unistd.h>
#include <getopt.h>

#include <set>
#include <mutex>
#include <condition_variable>
#include <thread>

#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>
#include <upnpdescription.h>
#include <netif.h>

#include <iostream>

static char *thisprog;
static char usage [] =
    "[--target/-t] : set search target (default: upnp:rootdevice)\n"
    "[--passive/-p] : no search: just listen to notify messages\n"
    "[--verbose/-v] : verbose\n"
" \n"
;

static void
Usage(void)
{
    fprintf(stderr, "%s: usage:\n%s", thisprog, usage);
    exit(1);
}

static int       op_flags;
#define OPT_t    0x1
#define OPT_v    0x2
#define OPT_p    0x4
static struct option long_options[] = {
    {"target", required_argument, 0, 't'},
    {"verbose", 0, 0, 'v'},
    {"passive", 0, 0, 'p'},
    {0, 0, 0, 0}
};


static int clctxt;

std::map<std::string, std::string> locations;
std::set<std::string> seenuuids;
std::mutex locations_mutex;
std::condition_variable locations_cv;
static bool searchdone;

static int mycallback(Upnp_EventType etyp, const void *evt, void *ctxt)
{
    static std::mutex log_mutex;
    //std::cerr << "mycallback: et " << etyp << " ctxt " << ctxt << "\n";
    UpnpDiscovery *dsp = (UpnpDiscovery*)evt;
    switch (etyp) {
    case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
        //std::cerr << "Alive: UDN [" << dsp->DeviceId << "] location [" <<
        //    dsp->Location << "]\n";
        goto dldesc;
    case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
    {
        std::lock_guard<std::mutex> lock(log_mutex);
        std::cerr << "ByeBye: UDN [" << dsp->DeviceId << "] location [" <<
            dsp->Location << "]\n";
    }
    break;
    case UPNP_DISCOVERY_SEARCH_RESULT:
        //std::cerr << "Search Result: UDN [" << dsp->DeviceId <<
        //  "] location [" << dsp->Location << "]\n";
        goto dldesc;
    case UPNP_DISCOVERY_SEARCH_TIMEOUT:
    {
        //std::cerr << "== Search Done == \n";
        std::unique_lock<std::mutex> loclock(locations_mutex);
        searchdone = true;
        locations_cv.notify_all();
    }
    break;
    default:
        std::cerr << "Received surprising event type " << etyp << "\n";
        break;
    }        
    return 0;

dldesc:
    if (op_flags & OPT_v) {
        std::lock_guard<std::mutex> lock(log_mutex);
        std::cout << "Discovery result: ErrCode [" << dsp->ErrCode << "]";
        std::cout << " Expires [" << dsp->Expires << "]";
        std::cout << " DeviceId [" << dsp->DeviceId << "]";
        if (dsp->DeviceType[0])
            std::cout << " DeviceType [" << dsp->DeviceType << "]";
        if (dsp->ServiceType[0])
            std::cout << " ServiceType [" << dsp->ServiceType << "]";
        if (dsp->ServiceVer[0])
            std::cout << " ServiceVer [" << dsp->ServiceVer << "]";
        std::cout << " Location " << dsp->Location << "]";
        if (dsp->Os[0])
            std::cout << " Os [" << dsp->Os << "]";
        if (dsp->Date[0])
            std::cout << " Date [" << dsp->Date << "]";
        if (dsp->Ext[0])
            std::cout << " Ext [" << dsp->Ext << "]";
        NetIF::IPAddr addr((struct sockaddr*)&dsp->DestAddr);
        std::cout << " Addr [" << addr.straddr() << "]";
        std::cout << "\n";
    }    
    std::unique_lock<std::mutex> loclock(locations_mutex);
    locations[dsp->DeviceId] = dsp->Location;
    locations_cv.notify_all();
    return 0;
}

void downloadAndShowDesc(std::string url)
{
    static std::mutex showMutex;
    std::string data;
    std::string ct;
    int success = UpnpDownloadUrlItem(url, data, ct);

    std::unique_lock<std::mutex> lock(showMutex);
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "UpnpDownloadUrlItem failed: for " << url << " code: "
                  << success << " " << UpnpGetErrorMessage(success) << "\n";
        return;
    }
    UPnPDeviceDesc desc(url, data);
    if (!desc.ok) {
        std::cout << "Description parse failed for " << url << "\n";
    } else {
        std::cout << " location " << url << " Got " << data.size() <<
            " bytes of description data. " << " friendly name: " <<
            desc.friendlyName << " UDN " << desc.UDN << "\n";
    }
}

int main(int argc, char **argv)
{
    thisprog = argv[0];
    std::string target{"upnp:rootdevice"};

    (void)op_flags;
    int ret;
    while ((ret = getopt_long(argc, argv, "t:vp",
                              long_options, NULL)) != -1) {
        switch (ret) {
        case 't': target = optarg;op_flags |= OPT_t; break;
        case 'v': op_flags |= OPT_v; break;
        case 'p': op_flags |= OPT_p; break;
        default:
            Usage();
        }
    }
    std::cerr << "target ["  << target << "]\n";
    if (optind != argc) 
        Usage();

    const char *cp = getenv("UPNP_LOGLEVEL");
    int level = cp ? atoi(cp) : UPNP_ERROR;
    if (level < 0)
        level = UPNP_ERROR;
    if (level > UPNP_ALL)
        level = UPNP_ALL;
    UpnpSetLogFileNames("", "");
    UpnpSetLogLevel(Upnp_LogLevel(level));
    UpnpInitLog();

    int success = UpnpInitWithOptions("*", 0, UPNP_FLAG_IPV6, UPNP_OPTION_END);
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "init failed: " << success << " " <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }

    UpnpClient_Handle chdl;
    success = UpnpRegisterClient(mycallback, &clctxt, &chdl);
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "register client failed: " << success << " " <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }

    if (op_flags & OPT_p) {
        op_flags |= OPT_v;
        pause();
    }
    int searchctxt;
    success = UpnpSearchAsync(chdl, 3, target.c_str(), &searchctxt);

    std::unique_lock<std::mutex> locklocs(locations_mutex);
    for (;;) {
        locations_cv.wait(locklocs);
        if (searchdone) {
            break;
        }
        for (auto it = locations.begin(); it != locations.end();) {
            if (seenuuids.find(it->first) != seenuuids.end()) {
                it = locations.erase(it);
                continue;
            }
            // Yep, leaking a bit for simplicity
            auto thr = new std::thread(downloadAndShowDesc, it->second);
            thr->detach();
            seenuuids.insert(it->first);
            it = locations.erase(it);
        }
    }
    // Possibly wait a bit for the stragglers
    sleep(2);
    UpnpFinish();
    return 0;
}
