
#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>

#include <iostream>
#include <unistd.h>

static int mycb(Upnp_EventType etyp, const void *evt, void *ctxt)
{
    std::cerr << "mycb: et " << etyp << " ctxt " << ctxt << "\n";
    std::cerr << "We should do something here...\n";
    return 0;
}

// Using a relative root path, not a good idea ! but simpler for the sample
std::string root("smallsamps");
std::string desc("smallsamps/dev-desc.xml");

int main(int argc, char **argv)
{

    UpnpSetLogFileNames("", "");
    UpnpSetLogLevel(UPNP_INFO);
    UpnpInitLog();

    int success = UpnpInit2("", 0);

    if (success != UPNP_E_SUCCESS) {
        std::cerr << "init failed: " << success <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }
    std::cout << "Init ok, address: " << UpnpGetServerIpAddress() << " port " <<
        UpnpGetServerPort() << "\n";

    success = UpnpSetWebServerRootDir(root.c_str());
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "UpnpSetWebServerRootDir failed: " << success <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }
    UpnpDevice_Handle hdl;
    success = UpnpRegisterRootDevice2(
        UPNPREG_FILENAME_DESC, desc.c_str(), 0, 0, mycb, 0, &hdl);
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "UpnpRegisterRootDevice2 failed: " << success <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }

    // Start the discovery advertisements. Only call once, this starts the loop.
    UpnpSendAdvertisement(hdl, 1800);
    
    while (true) {
        sleep(1000);
    }
    return 0;
}
