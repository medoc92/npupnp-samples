/* Copyright (C) 2017-2020 J.F.Dockes
 *
 * License: GPL 2.1
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

//
// UPnP events: discover device and service, then subscribe to the
// service events and forever print them.
//
// For simplicity, we only use root devices, not embedded ones.

#include <unistd.h>
#include <getopt.h>
#include <signal.h>

#include <mutex>
#include <condition_variable>
#include <cstring>
#include <iostream>
#include <map>
#include <vector>
#include <algorithm>

#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>
#include <upnpdescription.h>


void discardsigs(int)
{
    return;
}

static char *thisprog;
static char usage [] =
"Options:\n"
"  [--verbose/-v] : verbose\n"
"<device> can be specified as a friendly name or uuid. <service> can be a\n"
"partial match. Both are case-insensitive\n"
;

static void
Usage(void)
{
    std::cerr << "Usage: " << thisprog << " <device> <service> [<service>...]\n";
    std::cerr << usage << "\n";
    exit(1);
}

static int       op_flags;
#define OPT_v    0x2
static struct option long_options[] = {
    {"verbose", 0, 0, 'v'},
    {0, 0, 0, 0}
};

// Device and services as specified by the command line
static std::string udevice;
static std::vector<std::string> uservices;

// Client context for npupnp callbacks. Not really used as we're using
// global data.
static int clctxt;

// For waiting for the end of discovery
std::mutex search_mutex;
std::condition_variable search_cv;
static bool searchdone;

// Device and service details out from discovery
struct ServiceDetails {
    std::string service_type;
    std::string service_ctlurl;
    std::string service_evturl;
};
static std::vector<ServiceDetails> services;

static std::map<std::string, std::string> subscriptions;

static void dumpdisco(UpnpDiscovery *x)
{
    std::cerr << "Discovery event: "
              << " ErrCode " << UpnpDiscovery_get_ErrCode(x) 
              << " Expires " << UpnpDiscovery_get_Expires(x) 
              << " DeviceId [" << UpnpDiscovery_get_DeviceID_cstr(x) 
              << "] DeviceType [" << UpnpDiscovery_get_DeviceType_cstr(x) 
              << "] ServiceType [" << UpnpDiscovery_get_ServiceType_cstr(x) 
              << "] ServiceVer [" << UpnpDiscovery_get_ServiceVer_cstr(x) 
              << "] Location [" << UpnpDiscovery_get_Location_cstr(x) 
              << "] Os [" << UpnpDiscovery_get_Os_cstr(x) 
              << "] Date [" << UpnpDiscovery_get_Date_cstr(x) 
              << "] Ext [" << UpnpDiscovery_get_Ext_cstr(x)
              << "\n";
    // DestAddr
}

// Check if device found through discovery matches the user spec.
// We download the description, needed for matching the friendlyname.
static bool matchdevice(UpnpDiscovery *dsp, bool isbyebye)
{
    std::string data;
    std::string ct;
    UPnPDeviceDesc desc;
    
    if (!isbyebye) {
        int success = UpnpDownloadUrlItem(dsp->Location, data, ct);
        if (success != UPNP_E_SUCCESS) {
            std::cerr << "UpnpDownloadUrlItem failed with " << success <<
                " for uuid " << dsp->DeviceId << " location [" <<
                dsp->Location << "] : "<< UpnpGetErrorMessage(success) << "\n";
            dumpdisco(dsp);
            return false;
        }

        desc = UPnPDeviceDesc(dsp->Location, data);
        if (!desc.ok) {
            std::cerr << "Description parse failed: " << dsp->Location << "\n";
            return false;
        }
    }

    if (!strcasecmp(udevice.c_str(), desc.UDN.c_str()) ||
        !strcasecmp(udevice.c_str(), desc.friendlyName.c_str())) {
        if (isbyebye) {
            std::cerr << "Device exited\n";
            exit(0);
        }

        // Found device. Match services
        for (const auto& sdesc : desc.services) {
            for (const auto& uservice  : uservices) {
                if (strcasestr(sdesc.serviceType.c_str(), uservice.c_str())) {
                    std::cerr << "Got stype" << sdesc.serviceType << " cURL " <<
                        sdesc.controlURL << " eURL " << sdesc.eventSubURL <<"\n";
                    ServiceDetails n{sdesc.serviceType,
                                     desc.URLBase + sdesc.controlURL,
                                     desc.URLBase + sdesc.eventSubURL};
                    if (std::find_if(services.begin(), services.end(),
                                  [n](ServiceDetails& e) {
                                      return e.service_ctlurl ==
                                          n.service_ctlurl;}) == services.end()){
                        services.push_back(n);
                    }
                }
            }
        }
        return true;
    }
    
    return false;
}

// General npupnp callback. Async events of all types go through here.
static int mycallback(Upnp_EventType etyp, const void *evt, void *ctxt)
{
    std::unique_lock<std::mutex> loclock(search_mutex);
    switch (etyp) {
    case UPNP_DISCOVERY_ADVERTISEMENT_ALIVE:
    case UPNP_DISCOVERY_SEARCH_RESULT:
    {
        UpnpDiscovery *dsp = (UpnpDiscovery*)evt;
        matchdevice(dsp, false);
        if (services.size() == uservices.size()) {
            searchdone = true;
            search_cv.notify_all();
        }
    }
    break;

    case UPNP_DISCOVERY_ADVERTISEMENT_BYEBYE:
    {
        UpnpDiscovery *dsp = (UpnpDiscovery*)evt;
        matchdevice(dsp, true);
    }
    break;

    case UPNP_DISCOVERY_SEARCH_TIMEOUT:
    {
        searchdone = true;
        search_cv.notify_all();
    }
    break;

    case UPNP_EVENT_RECEIVED:
    {
        struct Upnp_Event *evp = (struct Upnp_Event *)evt;
        std::string uservice;
        auto it = subscriptions.find(evp->Sid);
        if (it == subscriptions.end()) {
            uservice = "UNKNOWN ??";
        } else {
            uservice = it->second;
        }
        std::cout << "Got upnp event for " << udevice << ":" << uservice <<
            " subscription " << evp->Sid <<
            " seq " << evp->EventKey << " Changed:\n";
        for (const auto& entry : evp->ChangedVariables) {
            std::cout << "[" << entry.first << "]->[" << entry.second << "]\n";
        }
        std::cout << "\n";
    }
    break;

    default:
        std::cerr << "Received surprising event type " << etyp << "\n";
        break;
    }        
    return 0;
}


int main(int argc, char **argv)
{
    thisprog = argv[0];

    (void)op_flags;
    int ret;
    while ((ret = getopt_long(argc, argv, "v", long_options, NULL)) != -1) {
        switch (ret) {
        case 'v': op_flags |= OPT_v; break;
        default:
            Usage();
        }
    }
    std::cerr << "optind " << optind << " argc " << argc <<"\n";
    if (optind > argc -2 ) 
        Usage();
    udevice = argv[optind++];

    while (optind < argc) {
        uservices.push_back(argv[optind++]);
    }
    
    signal(SIGINT, discardsigs);

    UpnpSetLogFileNames("", "");
    UpnpSetLogLevel(UPNP_ERROR);
    UpnpInitLog();

    int success = UpnpInitWithOptions("*", 0, UPNP_FLAG_IPV6, UPNP_OPTION_END);
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "init failed: " << success << " " <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }

    UpnpClient_Handle chdl;
    success = UpnpRegisterClient(mycallback, &clctxt, &chdl);
    if (success != UPNP_E_SUCCESS) {
        std::cerr << "register client failed: " << success << " " <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }

    // Discovery
    int searchctxt;
    success = UpnpSearchAsync(chdl, 3, "upnp:rootdevice", &searchctxt);
    std::cerr << "UpnpSearchAsync returned\n";
    {
        std::unique_lock<std::mutex> slock(search_mutex);
        if (!searchdone)
            search_cv.wait(slock);
        if (services.size() != uservices.size()) {
            std::cerr << "Device or some services not found\n";
            return 1;
        }
    
        // Subscribe and forever print events.
        int timeout = 3600;
        Upnp_SID subsid;
        for (unsigned int i = 0; i < services.size(); i++) {
            const auto& service{services[i]};
            int code = UpnpSubscribe(chdl, service.service_evturl.c_str(),
                                     &timeout, subsid);
            if (code != UPNP_E_SUCCESS) {
                std::cerr << "UpnpSubscribe failed. code " << code << ": " <<
                    UpnpGetErrorMessage(code) << "\n";
                return 1;
            }
            subscriptions[subsid] = uservices[i];
        }
    }

    pause();

    std::cerr << "Got signal\n";
    
    UpnpFinish();
    return 0;
}
