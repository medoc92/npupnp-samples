
#include <upnp.h>
#include <upnpdebug.h>
#include <upnptools.h>

#include <iostream>
#include <unistd.h>
#include <signal.h>

void discardsigs(int)
{
    return;
}

int main(int argc, char **argv)
{
    if (argc != 2) {
        std::cerr << "Usage: npupnp_init <ifname>\n";
        return 1;
    }
    signal(SIGINT, discardsigs);
    const char *ifname = argv[1];

    UpnpSetLogFileNames("", "");
    UpnpSetLogLevel(UPNP_INFO);
    UpnpInitLog();

    int port = 0;
    unsigned int flags = UPNP_FLAG_IPV6;

    int success = UpnpInitWithOptions(
        ifname, port, flags, UPNP_OPTION_NETWORK_WAIT, 5, UPNP_OPTION_END);

    if (success != UPNP_E_SUCCESS) {
        std::cerr << "init failed: " << success << " " <<
            UpnpGetErrorMessage(success) << "\n";
        return 1;
    }

    std::cout << "IPV4 address: " << UpnpGetServerIpAddress() << "\n";
    std::cout << "IPV6 address: " << UpnpGetServerIp6Address() << "\n";
    std::cout << "Port: " << UpnpGetServerPort() << "\n";

    pause();
    
    UpnpFinish();
    return 0;
}
