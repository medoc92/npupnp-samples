
#include <upnp.h>
#include <upnpdebug.h>

#include <iostream>

int main(int argc, char **argv)
{

    UpnpSetLogFileNames("/tmp/mylogfilename.txt", "");
    UpnpSetLogLevel(UPNP_INFO);
    UpnpInitLog();

    UpnpPrintf(UPNP_INFO, DOM, __FILE__, __LINE__, "my log message\n");
    
    int success = UpnpInit2("", 0);

    if (success != UPNP_E_SUCCESS) {
        std::cerr << "init failed: " << success << "\n";
    } else {
        std::cerr << "Init ok, address: " << UpnpGetServerIpAddress() << "\n";
    }

    return 0;
}
