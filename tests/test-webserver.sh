#!/bin/sh
# Exercise the libnpupnp webserver functions

fatal()
{
    echo $*
    exit 1
}

test -x ../webserver || fatal no webserver executable in parent directory

########### Create temporary directory and arrange for reasonably safe cleanup
tmpdir=`mktemp -d`
workdir=$tmpdir/test-webserver
mkdir $tmpdir/test-webserver || exit 1

cleanup()
{
    if test -n "$tmpdir";then
        if test -d $tmpdir/test-webserver/; then
            rm -f $tmpdir/test-webserver/*
            rmdir $tmpdir/test-webserver
            rmdir $tmpdir
        fi
    fi
    if test -n "$serverpid";then
        kill $serverpid
    fi
}
trap cleanup 0 2 15

########## Start WEB server and retrieve its pid/host/port values
statusfile=$workdir/status
(../webserver $workdir | (read line; echo $line > $statusfile))&
sleep 1

serverpid=`awk '{print $2}'` < $statusfile
host=`awk '{print $4}'` < $statusfile
port=`awk '{print $6}'` < $statusfile

echo "PID $serverpid HOST $host PORT $port"

########## Link some big file into the data directory: use a Linux kernel.
kpath=`ls /boot/vmlinuz*|tail -1`
kname=`basename $kpath`

inputdata=$workdir/$kname
ln -s $kpath $inputdata || exit 1

#echo testing with $inputdata

######### Test function for data transfers and range headers

outdata=$workdir/outdata
refdata=$workdir/refdata

test_transfer()
{
    url=$1
    curl -s -S -o $outdata $url
    cmp $inputdata $outdata || fatal failed for $url
    cp /dev/null $outdata

    for offset in 0 1 10 1024 1025 4096 4097 4098 1248729;do
        for sz in 1 3 11 1024 1025 40961;do
            endrange=`expr $offset + $sz - 1`
            curl -s -S -o $outdata $url -H "Range: bytes=${offset}-${endrange}"
            dd bs=1 if=${inputdata} skip=${offset} count=${sz} of=${refdata} \
               status=none
            cmp $refdata $outdata || \
                fatal failed for url $url offset $offset sz $sz
            cp /dev/null $outdata
        done
    done
}


#### Test vdir transfers
test_transfer http://$host:$port/$workdir/$kname
#### Test direct transfers
test_transfer http://$host:$port/$kname

###### Test perms
cp /etc/group $workdir || exit 1
chmod 0 $workdir/group || exit 1
for url in \
    http://$host:$port/$workdir/group \
        http://$host:$port/group
do
    curl -s -S -o $outdata $url
    grep -q '403 Forbidden' $outdata || fatal Transferred protected file $url
done
